<?php

namespace Modules\Content\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Modules\User\Contracts\Authentication;

class PosttypeMiddleware
{
    protected $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $slug = $request->route('postTypeObj')->slug;

        switch ($routeName) {
            case "admin.content.post.index" :
                $permission = "content.{$slug}.posts.index";
                break;
            case "admin.content.post.create" :
            case "admin.content.post.store" :
                $permission = "content.{$slug}.posts.create";
                break;
            case "admin.content.post.edit" :
            case "admin.content.post.update" :
                $permission = "content.{$slug}.posts.edit";
                break;
            case "admin.content.post.destroy" :
                $permission = "content.{$slug}.posts.destroy";
                break;
            case "admin.content.category.index" :
                $permission = "content.{$slug}.categories.index";
                break;
            case "admin.content.category.create" :
            case "admin.content.category.store" :
                $permission = "content.{$slug}.categories.create";
                break;
            case "admin.content.category.edit" :
            case "admin.content.category.update" :
                $permission = "content.{$slug}.categories.edit";
                break;
            case "admin.content.category.destroy" :
                $permission = "content.{$slug}.categories.destroy";
                break;
            default :
                // WTF Case ???
                $permission = "candoanythings";
                break;
        }

        if (!$this->auth->hasAccess($permission)) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}