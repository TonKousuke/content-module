<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/content'], function (Router $router) {
    // ==================== Route Params Bindings ====================
    $router->bind('post', function ($id) {
        return app('Modules\Content\Repositories\PostRepository')->find($id);
    });
    $router->bind('postCategory', function ($id) {
        return app('Modules\Content\Repositories\CategoryRepository')->find($id);
    });
    $router->bind('postTypeObj', function ($slug) {
        return app('Modules\Content\Repositories\PosttypeRepository')->findByAttributes(['slug' => $slug]);
    });
    $router->bind('posttype', function ($id) {
        return app('Modules\Content\Repositories\PosttypeRepository')->find($id);
    });

    // ========== !!! Post Management & Category Management Must be in this MIDDLEWARE !!! ==========
    $router->group(['middleware' => \Modules\Content\Http\Middleware\PosttypeMiddleware::class], function (Router $router) {
        // ========== Post Management ==========
        $router->get('{postTypeObj}/posts', [
            'as' => 'admin.content.post.index',
            'uses' => 'PostController@index',
        ]);
        $router->get('{postTypeObj}/posts/create', [
            'as' => 'admin.content.post.create',
            'uses' => 'PostController@create',
        ]);
        $router->post('{postTypeObj}/posts', [
            'as' => 'admin.content.post.store',
            'uses' => 'PostController@store',
        ]);
        $router->get('{postTypeObj}/posts/{post}/edit', [
            'as' => 'admin.content.post.edit',
            'uses' => 'PostController@edit',
        ]);
        $router->put('{postTypeObj}/posts/{post}', [
            'as' => 'admin.content.post.update',
            'uses' => 'PostController@update',
        ]);
        $router->delete('{postTypeObj}/posts/{post}', [
            'as' => 'admin.content.post.destroy',
            'uses' => 'PostController@destroy',
        ]);

        // ========== Category Management ==========
        $router->get('{postTypeObj}/categories', [
            'as' => 'admin.content.category.index',
            'uses' => 'CategoryController@index',
        ]);
        $router->get('{postTypeObj}/categories/create', [
            'as' => 'admin.content.category.create',
            'uses' => 'CategoryController@create',
        ]);
        $router->post('{postTypeObj}/categories', [
            'as' => 'admin.content.category.store',
            'uses' => 'CategoryController@store',
        ]);
        $router->get('{postTypeObj}/categories/{postCategory}/edit', [
            'as' => 'admin.content.category.edit',
            'uses' => 'CategoryController@edit',
        ]);
        $router->put('{postTypeObj}/categories/{postCategory}', [
            'as' => 'admin.content.category.update',
            'uses' => 'CategoryController@update',
        ]);
        $router->delete('{postTypeObj}/categories/{postCategory}', [
            'as' => 'admin.content.category.destroy',
            'uses' => 'CategoryController@destroy',
        ]);
    });
    // ========== !!! Post Management & Category Management Must be in this MIDDLEWARE !!! ==========

    // ========== Post Type Management ==========
    $router->get('posttypes', [
        'as' => 'admin.content.posttype.index',
        'uses' => 'PosttypeController@index',
        'middleware' => 'can:content.posttypes.index'
    ]);
    $router->get('posttypes/create', [
        'as' => 'admin.content.posttype.create',
        'uses' => 'PosttypeController@create',
        'middleware' => 'can:content.posttypes.create'
    ]);
    $router->post('posttypes', [
        'as' => 'admin.content.posttype.store',
        'uses' => 'PosttypeController@store',
        'middleware' => 'can:content.posttypes.create'
    ]);
    $router->get('posttypes/{posttype}/edit', [
        'as' => 'admin.content.posttype.edit',
        'uses' => 'PosttypeController@edit',
        'middleware' => 'can:content.posttypes.edit'
    ]);
    $router->put('posttypes/{posttype}', [
        'as' => 'admin.content.posttype.update',
        'uses' => 'PosttypeController@update',
        'middleware' => 'can:content.posttypes.edit'
    ]);
    $router->delete('posttypes/{posttype}', [
        'as' => 'admin.content.posttype.destroy',
        'uses' => 'PosttypeController@destroy',
        'middleware' => 'can:content.posttypes.destroy'
    ]);

// append


});
