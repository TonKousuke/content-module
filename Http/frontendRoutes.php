<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => 'content'], function (Router $router) {
    $router->bind('postCategoryObj', function ($slug) {
        return app('Modules\Content\Repositories\CategoryRepository')->findBySlug($slug);
    });

    $router->get('{postTypeObj}/posts/{post}-{slug}', [
        'as' => 'content.post.show',
        'uses' => 'PublicController@postShow',
    ]);

    $router->get('{postTypeObj}/posts', [
        'as' => 'content.post.index',
        'uses' => 'PublicController@postIndex',
    ]);

    $router->get('{postTypeObj}/categories/{postCategoryObj}', [
        'as' => 'content.category.index',
        'uses' => 'PublicController@categoryIndex',
    ]);

    $router->get('{postTypeObj}/tags/{slug}', [
        'as' => 'content.tag.index',
        'uses' => 'PublicController@tagIndex',
    ]);
});