<?php

namespace Modules\Content\Http\Controllers\Api;

// use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Content\Repositories\PostRepository;
use Modules\Content\Entities\Post;
use Modules\Content\Entities\PostType;
use Modules\Content\Transformers\PostApiTransformer;

class PostController extends Controller
{
    private $post;

    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }

    /**
     * @api {get} content/:posttype/posts Get Posts list
     * @apiDescription Get list of Posts by Posttype (e.g. blog, news, etc.)
     * @apiVersion 0.1.0
     * @apiName GetPosts
     * @apiGroup Content
     *
     * @apiParam {String} locale           Content Translation Locale
     * @apiParam {String} posttype         Content Posttype
     * @apiParam (Query string) {Number} [page=1]         Page Number
     * @apiParam (Query string) {Number} [perPage=10]     Items Per Page
     * @apiParam (Query string) {String} [orderBy=id]     OrderBy Field
     * @apiParam (Query string) {String} [sortOrder=desc] Sort Ordering
     * @apiParam (Query string) {String} [categoryId=]  Filter by CategoryId
     * @apiParam (Query string) {String} [tags=]  Filter by Tags Slug
     */
    public function index(Posttype $postTypeObj)
    {
        $posttype = $postTypeObj;
        // s(request()->input('orderBy')); die;
        $params = [
            'page'           => request()->input('page', 1),
            'perPage'        => request()->input('perPage', 10),
            'orderBy'        => request()->input('orderBy', 'id'),
            'sortOrder'      => request()->input('sortOrder', 'desc'),
            'categoryId'     => request()->input('categoryId'),
            'tag'            => request()->input('tags'),
            'posttype'       => $posttype,
            // 'status'         => request()->input('status', 1),
            // 'onScheduleOnly' => request()->input('onScheduleOnly', true),
            'status'         => 1,
            'onScheduleOnly' => true,
        ];
        try {
            $results = $this->post->search($params);
        } catch (\Exception $e) {
            // s($e->getMessage());
            // die;
            // return PostApiTransformer::collection(collect([]));
            return response()->json(['status' => 'error', 'code' => 400, 'message' => $e->getMessage()], 400);
        }

        // s($results->toArray());
        return PostApiTransformer::collection($results);
    }

    /**
     * @api {get} content/:posttype/posts/:id Get Post detail
     * @apiDescription Get Post Detail by unique ID
     * @apiVersion 0.1.0
     * @apiName GetPost
     * @apiGroup Content
     *
     * @apiParam {String} locale   Content Post Translation Locale
     * @apiParam {String} posttype Content Posttype
     * @apiParam {Number} id       Post unique ID
     */
    public function show(Posttype $postTypeObj, Post $post)
    {
        $posttype = $postTypeObj;

        if ($post->isPosttype($posttype) && $post->isPublishedAndOnSchedule()) {
            return new PostApiTransformer($post);
        }

        return response()->json(['status' => 'error', 'code' => 400, 'message' => 'Post Content not found.'], 400);
    }
}
