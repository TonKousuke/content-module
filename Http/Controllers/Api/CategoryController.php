<?php

namespace Modules\Content\Http\Controllers\Api;

// use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Content\Repositories\CategoryRepository;
use Modules\Content\Entities\Category;
use Modules\Content\Entities\PostType;
use Modules\Content\Transformers\CategoryHierarchyApiTransformer;
use Modules\Content\Transformers\CategoryApiTransformer;

class CategoryController extends Controller
{
    private $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * @api {get} content/:posttype/categories Get Categories list
     * @apiDescription Get list of Categories by Posttype (e.g. blog, news, etc.)
     * @apiVersion 0.1.0
     * @apiName GetCategories
     * @apiGroup Content
     *
     * @apiParam {String} locale           Content Translation Locale
     * @apiParam {String} posttype         Content Posttype
     */
    public function index(Posttype $postTypeObj)
    {
        $posttype = $postTypeObj;

        try {
            $results = $this->category->getByPosttype($posttype);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 400, 'message' => $e->getMessage()], 400);
        }

        $categoriesHierarchy = $results->toHierarchy();

        $categoriesHierarchyTransformed = CategoryHierarchyApiTransformer::collection($categoriesHierarchy);
        $response = array_values($categoriesHierarchyTransformed->collection->toArray());

        return response()->json(['data' => $response]);
    }

    /**
     * @api {get} content/:posttype/categories/:id Get Category detail
     * @apiDescription Get Category Detail by unique ID
     * @apiVersion 0.1.0
     * @apiName GetCategory
     * @apiGroup Content
     *
     * @apiParam {String} locale   Content Post Translation Locale
     * @apiParam {String} posttype Content Posttype
     * @apiParam {Number} id       Category unique ID
     */
    public function show(Posttype $postTypeObj, Category $postCategory)
    {
        $posttype = $postTypeObj;

        if ($postCategory->isPosttype($posttype)) {
            return new CategoryApiTransformer($postCategory);
        }

        return response()->json(['status' => 'error', 'code' => 400, 'message' => 'Category Content not found.'], 400);
    }
}
