<?php

namespace Modules\Content\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Content\Entities\Posttype;
use Modules\Content\Http\Requests\CreatePosttypeRequest;
use Modules\Content\Http\Requests\UpdatePosttypeRequest;
use Modules\Content\Repositories\PosttypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PosttypeController extends AdminBaseController
{
    /**
     * @var PosttypeRepository
     */
    private $posttype;

    public function __construct(PosttypeRepository $posttype)
    {
        parent::__construct();

        $this->posttype = $posttype;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posttypes = $this->posttype->all();

        return view('content::admin.posttypes.index', compact('posttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('content::admin.posttypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePosttypeRequest $request
     * @return Response
     */
    public function store(CreatePosttypeRequest $request)
    {
        $this->posttype->create($request->all());

        return redirect()->route('admin.content.posttype.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('content::posttypes.title.posttypes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Posttype $posttype
     * @return Response
     */
    public function edit(Posttype $posttype)
    {
        return view('content::admin.posttypes.edit', compact('posttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Posttype $posttype
     * @param  UpdatePosttypeRequest $request
     * @return Response
     */
    public function update(Posttype $posttype, UpdatePosttypeRequest $request)
    {
        $this->posttype->update($posttype, $request->all());

        return redirect()->route('admin.content.posttype.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('content::posttypes.title.posttypes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Posttype $posttype
     * @return Response
     */
    public function destroy(Posttype $posttype)
    {
        $this->posttype->destroy($posttype);

        return redirect()->route('admin.content.posttype.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('content::posttypes.title.posttypes')]));
    }
}
