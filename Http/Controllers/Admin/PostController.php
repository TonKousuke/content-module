<?php

namespace Modules\Content\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Content\Entities\Post;
use Modules\Content\Entities\Posttype;
use Modules\Content\Http\Requests\CreatePostRequest;
use Modules\Content\Http\Requests\UpdatePostRequest;
use Modules\Content\Repositories\PostRepository;
use Modules\Content\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PostController extends AdminBaseController
{
    private $post, $category, $statusArr;

    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        parent::__construct();

        $this->post = $post;
        $this->category = $category;

        $this->statusArr = Post::STATUS_ARR;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Posttype $postTypeObj)
    {
        $posttype = $postTypeObj;
        $posts = $this->post->getByPosttype($postTypeObj);

        return view('content::admin.posts.index', compact('posts', 'posttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Posttype $postTypeObj)
    {
        $posttype = $postTypeObj;

        $categories = $this->category->getByPosttype($postTypeObj->slug);
        $selectedCat = [];
        $statusArr = $this->statusArr;

        return view('content::admin.posts.create', compact('posttype', 'categories', 'selectedCat', 'statusArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePostRequest $request
     * @return Response
     */
    public function store(Posttype $postTypeObj, CreatePostRequest $request)
    {
        $input = $request->all();
        $input['posttype_id'] = $postTypeObj->id;

        // - Create new Post & Translations
        // - Sync Post with Categories
        $post = $this->post->create($input);

        return redirect()->route('admin.content.post.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('content::posts.title.posts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post $post
     * @return Response
     */
    public function edit(Posttype $postTypeObj, Post $post)
    {
        $posttype = $postTypeObj;

        if (!$post->isPosttype($posttype)) {
            abort(404);
        }

        $categories = $this->category->getByPosttype($postTypeObj->slug);
        $selectedCat = [];

        $postCategories = $post->categories;
        if (!$postCategories->isEmpty()) {
            $selectedCat = $postCategories->pluck('id')->toArray();
        }

        $statusArr = $this->statusArr;

        return view('content::admin.posts.edit', compact('post', 'posttype', 'categories', 'selectedCat', 'statusArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Post $post
     * @param  UpdatePostRequest $request
     * @return Response
     */
    public function update(Posttype $postTypeObj, Post $post, UpdatePostRequest $request)
    {
        if (!$post->isPosttype($postTypeObj)) {
            abort(404);
        }

        // Update Post & Translations
        // Sync Post with Categories
        $this->post->update($post, $request->all());

        return redirect()->route('admin.content.post.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('content::posts.title.posts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return Response
     */
    public function destroy(Posttype $postTypeObj, Post $post)
    {
        if (!$post->isPosttype($postTypeObj)) {
            abort(404);
        }
        $this->post->destroy($post);

        return redirect()->route('admin.content.post.index', [$postTypeObj->slug])
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('content::posts.title.posts')]));
    }
}
