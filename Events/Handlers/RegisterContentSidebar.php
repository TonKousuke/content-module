<?php

namespace Modules\Content\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterContentSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item('Content Post Type', function (Item $item) {
                // $item->icon('fa fa-shopping-bag');
                $item->authorize(
                    $this->auth->hasAccess('content.posttypes.index')
                );
                $item->route('admin.content.posttype.index');
            });

            try {
                $posttypes = \Modules\Content\Entities\Posttype::get();
                // $posttypes = app('ContentPosttypesCollection');
            } catch (\Exception $e) {
                // Something error??
                $posttypes = NULL;
            }

            if (!empty($posttypes) && !$posttypes->isEmpty()) {
                foreach ($posttypes as $posttype) {
                    $group->item($posttype->name, function (Item $item) use ($posttype) {
                        // $item->icon('fa fa-shopping-bag');
                        $item->authorize(
                            $this->auth->hasAccess("content.{$posttype->slug}.posts.index")
                        );

                        $item->item(str_plural($posttype->name), function (Item $item) use ($posttype) {
                            // $item->icon('fa fa-shopping-bag');
                            $item->route('admin.content.post.index', [$posttype->slug]);
                            $item->authorize(
                                $this->auth->hasAccess("content.{$posttype->slug}.posts.index")
                            );
                        });
                        $item->item('Category', function (Item $item) use ($posttype) {
                            // $item->icon('fa fa-shopping-bag');
                            $item->route('admin.content.category.index', [$posttype->slug]);
                            $item->authorize(
                                $this->auth->hasAccess("content.{$posttype->slug}.categories.index")
                            );
                        });
                    });
                }
            }
        });

        return $menu;
    }
}
