<?php

namespace Modules\Content\Repositories\Eloquent;

use Modules\Content\Entities\Category;
use Modules\Content\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
    public function getByPosttype($posttype)
    {
        if (!$posttype instanceof Posttype && is_string($posttype)) {
            $posttypeRepo = app('Modules\Content\Repositories\PosttypeRepository');
            $posttype = $posttypeRepo->findBySlug($posttype);
        }

        if (empty($posttype) or empty($posttype->getKey())) {
            throw new \Exception("Posttype not found.");
        }

        $posttypeId = $posttype->id;
        $orderColumn = 'lft';
        $ordered = 'ASC';

        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->where('posttype_id', $posttypeId)->orderBy($orderColumn, $ordered)->get();
        }

        return $this->model->orderBy($orderColumn, $ordered)->where('posttype_id', $posttypeId)->get();
    }

    // !!!!! WARNING - Baum Bug !!! - Please Avoid to use makeChildOf Method !!!!!
    // public function makeChildOf(Category $child, Category $parent)
    // {
    //     $child->makeChildOf($parent);
    //     \Modules\Content\Entities\Category::rebuild(true);
    // }
}
