<?php

namespace Modules\Content\Repositories\Eloquent;

use Modules\Content\Repositories\PostRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Content\Events\PostIsCreating;
use Modules\Content\Events\PostWasCreated;
use Modules\Content\Events\PostIsUpdating;
use Modules\Content\Events\PostWasUpdated;
use Modules\Content\Events\PostWasDeleted;
use Modules\Content\Entities\Posttype;

class EloquentPostRepository extends EloquentBaseRepository implements PostRepository
{
    public function search($params = [])
    {
        $defaultParams = [
            'page'           => 1,
            'perPage'        => -1,
            'orderBy'        => 'id',
            'sortOrder'      => 'asc',
            'posttype'       => null,
            'categoryId'     => null,
            'tags'            => null,
            // 'status'      => -1,
            'status'         => array_get($params, 'status', -1),
            'onScheduleOnly' => false,
        ];
        $params = array_merge($defaultParams, array_filter($params));

        // Add params 'page' to request input (for paginate function)
        request()->merge(['page' => $params['page']]);

        // Build Query
        $query = $this->model->query();

        if (method_exists($this->model, 'translations')) {
            $query = $query->with('translations');
        }

        // Get Posttype Obj, Search By Posttype
        $posttype = $params['posttype'];

        if (!empty($posttype)) {
            if (!$posttype instanceof Posttype && is_string($posttype)) {
                $posttypeRepo = app('Modules\Content\Repositories\PosttypeRepository');
                $posttype = $posttypeRepo->findBySlug($posttype);
            }
            if (empty($posttype) or empty($posttype->getKey())) {
                throw new \Exception("Posttype not found.");
            }
            $query->where('posttype_id', $posttype->id);
        }

        // Filter by post status
        if ($params['status'] !== -1) {
            $query->status($params['status']);
        }

        // Filter only post that on schedule time
        if ($params['onScheduleOnly']) {
            $query->onSchedule();
        }

        // Filter by post in categoryId
        if (!empty($params['categoryId'])) {
            $query->whereHas('categories', function($q) use ($params) {
                $q->where('id', $params['categoryId']);
            });
        }

        // Filter by tag slug
        if (!empty($params['tags'])) {
            $query->whereTag($params['tags']);
        }

        // Filter by Condition ....
        // if () {
        //     $query = $query->where($field, $value);
        // }

        // Ordering
        $query->orderBy($params['orderBy'], $params['sortOrder']);

        // // Paginate
        // if ($params['perPage'] !== -1) {
        //     $skip = ($params['page'] - 1) * $params['perPage'];
        //     $take = $params['perPage'];
        //     $query->skip($skip)->take($take);
        // }

        // // Return Results - Collection of Eloquent
        // return $query->get();

        // Paginate
        if ($params['perPage'] !== -1) {
            // Return Results - Collection of Eloquent with Pagination
            return $query->paginate($params['perPage']);
        }

        // Return Results - Collection of Eloquent
        return $query->get();
    }

    public function getByPosttype($posttype)
    {
        return $this->search(['posttype' => $posttype]);
    }

    /**
     * Create a post
     * @param  array $data
     * @return Post
     */
    public function create($data)
    {
        event($event = new PostIsCreating($data));

        \DB::beginTransaction();

        // Create new Post & Translations
        $post = $this->model->create($event->getAttributes());

        // Sync Post with Categories
        $categoryIds = $event->getAttribute('categories', []);
        if (!empty($categoryIds)) {
            $post->categories()->sync($categoryIds);
        }

        // Set Tags
        $post->setTags(array_get($data, 'tags'));

        \DB::commit();

        event(new PostWasCreated($post, $data));

        return $post;
    }

    /**
     * Update a resource
     * @param $post
     * @param  array $data
     * @return mixed
     */
    public function update($post, $data)
    {
        event($event = new PostIsUpdating($post, $data));

        \DB::beginTransaction();

        // Update Post & Translations
        $post->update($event->getAttributes());

        // Sync Post with Categories
        $categoryIds = $event->getAttribute('categories', []);
        $post->categories()->sync($categoryIds);

        // Set Tags
        $post->setTags(array_get($data, 'tags'));

        \DB::commit();

        event(new PostWasUpdated($post, $data));

        return $post;
    }

    public function destroy($model)
    {
        // Detach Category
        $model->categories()->detach();

        // untag
        $model->untag();

        event(new PostWasDeleted($model->id, get_class($model)));

        return $model->delete();
    }
}
