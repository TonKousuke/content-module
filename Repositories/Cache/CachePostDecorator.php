<?php

namespace Modules\Content\Repositories\Cache;

use Modules\Content\Repositories\PostRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePostDecorator extends BaseCacheDecorator implements PostRepository
{
    public function __construct(PostRepository $post)
    {
        parent::__construct();
        $this->entityName = 'content.posts';
        $this->repository = $post;
    }
}
