<?php

namespace Modules\Content\Repositories\Cache;

use Modules\Content\Repositories\PosttypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePosttypeDecorator extends BaseCacheDecorator implements PosttypeRepository
{
    public function __construct(PosttypeRepository $posttype)
    {
        parent::__construct();
        $this->entityName = 'content.posttypes';
        $this->repository = $posttype;
    }
}
