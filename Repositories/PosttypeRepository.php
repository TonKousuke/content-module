<?php

namespace Modules\Content\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PosttypeRepository extends BaseRepository
{
}
