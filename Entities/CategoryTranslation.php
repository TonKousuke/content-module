<?php

namespace Modules\Content\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'detail'];
    protected $table = 'content__category_translations';
}
