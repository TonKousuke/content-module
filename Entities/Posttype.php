<?php

namespace Modules\Content\Entities;

use Illuminate\Database\Eloquent\Model;

class Posttype extends Model
{
    protected $table = 'content__posttypes';
    protected $fillable = ['name', 'slug'];
    public $timestamps = false;

    public function setSlugAttribute($val) {
        $this->attributes['slug'] = strtolower(preg_replace('/[^A-Za-z0-9\-]/u', '-', str_replace('&', '-and-', $val)));
    }
}
