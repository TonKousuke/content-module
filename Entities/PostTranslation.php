<?php

namespace Modules\Content\Entities;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'short_detail', 'detail'];
    protected $table = 'content__post_translations';

    public function setSlugAttribute($val) {
        $this->attributes['slug'] = strtolower(preg_replace('/[^A-Za-z0-9ก-๙\-]/u', '-', str_replace('&', '-and-', $val)));
    }
}
