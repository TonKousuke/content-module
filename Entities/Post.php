<?php

namespace Modules\Content\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

use Modules\Tag\Contracts\TaggableInterface;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Tag\Traits\TaggableTrait;

class Post extends Model implements TaggableInterface
{
    use Translatable, MediaRelation, NamespacedEntity, TaggableTrait;

    public $translationModel = 'Modules\Content\Entities\PostTranslation';
    public $useTranslationFallback = true;

    protected $table = 'content__posts';
    public $translatedAttributes = ['title', 'slug', 'short_detail', 'detail'];
    protected $fillable = ['title', 'slug', 'short_detail', 'detail', 'posttype_id', 'status', 'start_date', 'end_date'];

    const STATUS_ARR = [
        0 => 'Draft',
        1 => 'Publish',
    ];

    // ========== Relations ==========
    public function posttype()
    {
        return $this->belongsTo(\Modules\Content\Entities\Posttype::class);
    }

    public function categories()
    {
        return $this->belongsToMany(\Modules\Content\Entities\Category::class, 'content__post_category');
    }

    // ========== Query Scope ==========
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopePublish($query)
    {
        return $query->status(array_search('Publish', static::STATUS_ARR));
    }

    public function scopeDraft($query)
    {
        return $query->status(array_search('Draft', static::STATUS_ARR));
    }

    public function scopeOnSchedule($query)
    {
        $now = new \DateTime();

        return $query->where(function($q) use($now) {
            $q->whereNull('start_date')->orWhere('start_date', '<=', $now);
        })->where(function($q) use($now) {
            $q->whereNull('end_date')->orWhere('end_date', '>=', $now);
        });
    }

    // ========== Getters & Setters ==========
    public function getPosttypeSlugAttribute()
    {
        return $this->posttype->slug;
    }

    public function getPermalinkAttribute()
    {
        return route('content.post.show', [$this->posttypeSlug, $this->id, $this->slug]);
    }

    public function getStatusTextAttribute()
    {
        return static::STATUS_ARR[$this->status];
    }

    public function getThumbnailAttribute()
    {
        $image = $this->filesByZone('thumbnail')->first();
        return (empty($image)) ? '' : asset($image->getOriginal('path'));
    }

    public function getSquareThumbnailAttribute()
    {
        $image = $this->filesByZone('thumbnail')->first();
        return (empty($image)) ? '' : \Imagy::getThumbnail($image->path, 'squareThumb');
    }

    public function getRectangleThumbnailAttribute()
    {
        $image = $this->filesByZone('thumbnail')->first();
        return (empty($image)) ? '' : \Imagy::getThumbnail($image->path, 'rectangleThumb');
    }

    // ========== Other Utilities ==========
    public function isPosttype($posttype)
    {
        if (!$posttype instanceof Posttype) {
            $posttypeRepo = app('Modules\Content\Repositories\PosttypeRepository');
            $posttype = $posttypeRepo->findBySlug($posttype);
        }

        if (!empty($posttype) && !empty($posttype->getKey())) {
            if ($posttype->id == $this->posttype_id) {
                return true;
            }
        }

        return false;
    }

    public function isPublishedAndOnSchedule()
    {
        if ($this->statusText != 'Publish') {
            return false;
        }

        $now = date('Y-m-d H:i:s');
        if (!empty($this->start_date) or !empty($this->end_date)) {
            if (!empty($this->start_date) && $this->start_date > $now) {
                return false;
            }
            if (!empty($this->end_date) && $this->end_date < $now) {
                return false;
            }
        }

        return true;
    }
}
