<?php

namespace Modules\Content\Tests;

class EloquentPostTest extends BaseContentTestCase
{
    /** @test */
    public function it_can_check_posttype()
    {
        // Prepare Blog and News Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);
        $posttypeNews = $this->createPosttype(['name' => 'News', 'slug' => 'news']);

        $blog = $this->createPostInPosttype($posttypeBlog);
        $news = $this->createPostInPosttype($posttypeNews);

        $this->assertFalse($blog->isPosttype('news'));
        $this->assertTrue($blog->isPosttype('blog'));
        $this->assertFalse($blog->isPosttype('promotion'));
        $this->assertTrue($news->isPosttype('news'));
        $this->assertFalse($news->isPosttype('blog'));
        $this->assertFalse($news->isPosttype('promotion'));
    }

    /** @test */
    public function it_can_check_published_and_on_schedule_post()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Create Post
        $todayDateTime = date('Y-m-d H:i:s');
        $nextWeekDateTime = date('Y-m-d H:i:s', strtotime('+7days'));
        $yesterdayDateTime = date('Y-m-d H:i:s', strtotime('yesterday'));
        $tomorrowDateTime = date('Y-m-d H:i:s', strtotime('tomorrow'));
        // $next6daysDateTime = date('Y-m-d H:i:s', strtotime('+6days'));
        // $next8daysDateTime = date('Y-m-d H:i:s', strtotime('+8days'));
        $lastWeekDatetime = date('Y-m-d H:i:s', strtotime('-7days'));

        // Create Blog Post
        $post1 = $this->createPostInPosttype($posttypeBlog); // on schedule
        $post2 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime]); // on schedule
        $post3 = $this->createPostInPosttype($posttypeBlog, ['end_date' => $yesterdayDateTime]);
        $post4 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $tomorrowDateTime]);
        $post5 = $this->createPostInPosttype($posttypeBlog, ['end_date' => $tomorrowDateTime]);  // on schedule
        $post6 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime, 'end_date' => $nextWeekDateTime]); // on schedule
        $post7 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $tomorrowDateTime, 'end_date' => $nextWeekDateTime]); 
        $post8 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $lastWeekDatetime, 'end_date' => $tomorrowDateTime]); // on schedule
        $post9 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $lastWeekDatetime, 'end_date' => $yesterdayDateTime]);
        $post10 = $this->createPostInPosttype($posttypeBlog, ['status' => 0]);
        $post11 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime, 'status' => 0]);
        $post12 = $this->createPostInPosttype($posttypeBlog, ['end_date' => $tomorrowDateTime, 'status' => 0]);
        $post13 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime, 'end_date' => $nextWeekDateTime, 'status' => 0]);
        $post14 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $lastWeekDatetime, 'end_date' => $tomorrowDateTime, 'status' => 0]);

        $this->assertTrue($post1->isPublishedAndOnSchedule());
        $this->assertTrue($post2->isPublishedAndOnSchedule());
        $this->assertFalse($post3->isPublishedAndOnSchedule());
        $this->assertFalse($post4->isPublishedAndOnSchedule());
        $this->assertTrue($post5->isPublishedAndOnSchedule());
        $this->assertTrue($post6->isPublishedAndOnSchedule());
        $this->assertFalse($post7->isPublishedAndOnSchedule());
        $this->assertTrue($post8->isPublishedAndOnSchedule());
        $this->assertFalse($post9->isPublishedAndOnSchedule());
        $this->assertFalse($post10->isPublishedAndOnSchedule());
        $this->assertFalse($post11->isPublishedAndOnSchedule());
        $this->assertFalse($post12->isPublishedAndOnSchedule());
        $this->assertFalse($post13->isPublishedAndOnSchedule());
        $this->assertFalse($post14->isPublishedAndOnSchedule());
    }
}