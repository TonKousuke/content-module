<?php

namespace Modules\Content\Tests;

use Faker\Factory;
use Illuminate\Support\Str;
use Modules\Content\Entities\Posttype;
use Modules\Content\Entities\Category;
use Nwidart\Modules\LaravelModulesServiceProvider;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider;
use Modules\Core\Providers\CoreServiceProvider;
use Modules\Media\Image\ImageServiceProvider;
use Modules\Media\Providers\MediaServiceProvider;
use Modules\Content\Providers\ContentServiceProvider;
use Modules\Tag\Providers\TagServiceProvider;
// use Maatwebsite\Sidebar\SidebarServiceProvider;

abstract class BaseContentTestCase extends \Orchestra\Testbench\TestCase
{
    protected $post, $posttype, $category;

    public function setUp()
    {
        parent::setUp();

        $this->resetDatabase();
        $this->post = app('Modules\Content\Repositories\PostRepository');
        $this->posttype = app('Modules\Content\Repositories\PosttypeRepository');
        $this->category = app('Modules\Content\Repositories\CategoryRepository');
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['path.base'] = __DIR__ . '/..';
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', array(
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ));
        $app['config']->set('translatable.locales', ['en', 'th']);
    }

    private function resetDatabase()
    {
        // Makes sure the migrations table is created
        $this->artisan('migrate', [
            '--database' => 'sqlite',
        ]);
        // We empty all tables
        $this->artisan('migrate:reset', [
            '--database' => 'sqlite',
        ]);
        // Migrate
        $this->artisan('migrate', [
            '--database' => 'sqlite',
        ]);
        $this->artisan('migrate', [
            '--database' => 'sqlite',
            '--path'     => 'Modules/Media/Database/Migrations',
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelModulesServiceProvider::class,
            LaravelLocalizationServiceProvider::class,
            CoreServiceProvider::class,
            ImageServiceProvider::class,
            TagServiceProvider::class,
            MediaServiceProvider::class,
            ContentServiceProvider::class,
            // SidebarServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'LaravelLocalization' => LaravelLocalization::class,
        ];
    }


    ///////////////////////////////////////////////////////////////////////////

    public function createPosttype($data)
    {
        $posttype = Posttype::create($data);
        return $posttype;
    }

    public function createPostInPosttype(Posttype $posttype, $params = [])
    {
        $defaultParams = [
            'start_date' => null,
            'end_date'   => null,
            'status'     => 1,
            'categories' => [],
            'tags'       => [],
        ];
        $params = array_merge($defaultParams, $params);

        $faker = Factory::create();

        $title = implode(' ', $faker->words(3));
        $titleEn = $title . ' EN';
        $titleTh = $title . ' TH';

        $data = [
            'en' => [
                'title'        => $titleEn,
                'slug'         => Str::slug($titleEn),
                'short_detail' => $faker->text(100),
                'detail'       => $faker->paragraph(),
            ],
            'th' => [
                'title'        => $titleTh,
                'slug'         => Str::slug($titleTh),
                'short_detail' => $faker->text(100),
                'detail'       => $faker->paragraph(),
            ],
            'posttype_id' => $posttype->id,
            'status'      => $params['status'],
            'start_date'  => $params['start_date'],
            'end_date'    => $params['end_date'],
            'categories'  => $params['categories'],
            'tags'        => $params['tags'],
        ];

        return $this->post->create($data);
    }

    public function createCategoryInPosttype(Posttype $posttype, $parentId = null)
    {
        $faker = Factory::create();

        $title = implode(' ', $faker->words(2));
        $titleEn = $title . ' EN';
        $titleTh = $title . ' TH';

        $data = [
            'en' => [
                'title'  => $titleEn,
                'slug'   => Str::slug($titleEn),
                'detail' => $faker->text(80),
            ],
            'th' => [
                'title'  => $titleTh,
                'slug'   => Str::slug($titleTh),
                'detail' => $faker->text(80),
            ],
            'posttype_id' => $posttype->id,
            'parent_id' => $parentId
        ];

        return $this->category->create($data);
    }

}