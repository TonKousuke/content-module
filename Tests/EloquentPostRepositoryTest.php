<?php

namespace Modules\Content\Tests;

class EloquentPostRepositoryTest extends BaseContentTestCase
{
    /** @test */
    public function it_can_get_posts_by_each_posttype()
    {
        // Prepare Blog and News Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);
        $posttypeNews = $this->createPosttype(['name' => 'News', 'slug' => 'news']);

        // Create 3 Blogs and 4 news
        for ($i=0; $i<3; $i++) {
            $this->createPostInPosttype($posttypeBlog);
        }
        for ($i=0; $i<4; $i++) {
            $this->createPostInPosttype($posttypeNews);
        }

        // Test - Get Post By Posttype
        $blogPosts = $this->post->getByPosttype('blog');
        $newsPosts = $this->post->getByPosttype('news');

        $this->assertEquals(3, $blogPosts->count());
        $this->assertEquals(4, $newsPosts->count());
    }

    /** @test */
    public function it_can_search_each_posttype_and_get_paginate_results()
    {
        // Prepare Blog and News Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);
        $posttypeNews = $this->createPosttype(['name' => 'News', 'slug' => 'news']);

        // Create 14 Blogs and 8 news
        for ($i=0; $i<14; $i++) {
            $this->createPostInPosttype($posttypeBlog);
        }

        for ($i=0; $i<8; $i++) {
            $this->createPostInPosttype($posttypeNews);
        }

        $newsPostsAll   = $this->post->search(['posttype' => 'news']);
        $newsPostsPage1 = $this->post->search(['page' => 1, 'perPage' => 5, 'posttype' => 'news']);
        $newsPostsPage2 = $this->post->search(['page' => 2, 'perPage' => 5, 'posttype' => 'news']);
        $newsPostsPage3 = $this->post->search(['page' => 3, 'perPage' => 5, 'posttype' => 'news']);

        $this->assertEquals(8, $newsPostsAll->count());
        $this->assertEquals(5, $newsPostsPage1->count());
        $this->assertEquals(3, $newsPostsPage2->count());
        $this->assertEquals(0, $newsPostsPage3->count());

        $blogPostsAll   = $this->post->search(['page' => 1, 'posttype' => 'blog']);
        $blogPostsPage1 = $this->post->search(['page' => 1, 'perPage' => 5, 'posttype' => 'blog']);
        $blogPostsPage2 = $this->post->search(['page' => 2, 'perPage' => 5, 'posttype' => 'blog']);
        $blogPostsPage3 = $this->post->search(['page' => 3, 'perPage' => 5, 'posttype' => 'blog']);
        $blogPostsPage4 = $this->post->search(['page' => 4, 'perPage' => 5, 'posttype' => 'blog']);

        $this->assertEquals(14, $blogPostsAll->count());
        $this->assertEquals(5, $blogPostsPage1->count());
        $this->assertEquals(5, $blogPostsPage2->count());
        $this->assertEquals(4, $blogPostsPage3->count());
        $this->assertEquals(0, $blogPostsPage4->count());
    }

    /** @test */
    public function it_can_filter_posts_by_publish_status()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Create 5 Publish Blogs and 2 Draft Blog
        $draftStatus = 0;
        $publishStatus = 1;
        for ($i=0; $i<5; $i++) {
            $this->createPostInPosttype($posttypeBlog, ['status' => $publishStatus]);
        }
        for ($i=0; $i<2; $i++) {
            $this->createPostInPosttype($posttypeBlog, ['status' => $draftStatus]);
        }

        // Test - Get Post By Posttype
        $blogPosts = $this->post->search([
            'status' => $publishStatus,
            'posttype' => $posttypeBlog,
        ]);

        $draftBlogPosts = $this->post->search([
            'status' => $draftStatus,
            'posttype' => $posttypeBlog,
        ]);

        $this->assertEquals(5, $blogPosts->count());
        $this->assertEquals(2, $draftBlogPosts->count());
    }

    /** @test */
    public function it_can_filter_posts_by_publish_datetime()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Create Post
        $todayDateTime = date('Y-m-d H:i:s');
        $nextWeekDateTime = date('Y-m-d H:i:s', strtotime('+7days'));
        $yesterdayDateTime = date('Y-m-d H:i:s', strtotime('yesterday'));
        $tomorrowDateTime = date('Y-m-d H:i:s', strtotime('tomorrow'));
        // $next6daysDateTime = date('Y-m-d H:i:s', strtotime('+6days'));
        // $next8daysDateTime = date('Y-m-d H:i:s', strtotime('+8days'));
        $lastWeekDatetime = date('Y-m-d H:i:s', strtotime('-7days'));

        // Create Blog Post
        $post1 = $this->createPostInPosttype($posttypeBlog); // on schedule
        $post2 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime]); // on schedule
        $post3 = $this->createPostInPosttype($posttypeBlog, ['end_date' => $yesterdayDateTime]);
        $post4 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $tomorrowDateTime]);
        $post5 = $this->createPostInPosttype($posttypeBlog, ['end_date' => $tomorrowDateTime]);  // on schedule
        $post6 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $yesterdayDateTime, 'end_date' => $nextWeekDateTime]); // on schedule
        $post7 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $tomorrowDateTime, 'end_date' => $nextWeekDateTime]); 
        $post8 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $lastWeekDatetime, 'end_date' => $tomorrowDateTime]); // on schedule
        $post9 = $this->createPostInPosttype($posttypeBlog, ['start_date' => $lastWeekDatetime, 'end_date' => $yesterdayDateTime]);

        // No Condition, get all blog posts
        $rs1 = $this->post->search(['posttype' => 'blog']);
        $rs2 = $this->post->search(['posttype' => 'blog', 'onScheduleOnly' => true]);

        $this->assertEquals(9, $rs1->count());
        $this->assertEquals(5, $rs2->count());

        $this->assertContains($post1->id, $rs2->pluck('id')->toArray());
        $this->assertContains($post2->id, $rs2->pluck('id')->toArray());
        $this->assertNotContains($post3->id, $rs2->pluck('id')->toArray());
        $this->assertNotContains($post4->id, $rs2->pluck('id')->toArray());
        $this->assertContains($post5->id, $rs2->pluck('id')->toArray());
        $this->assertContains($post6->id, $rs2->pluck('id')->toArray());
        $this->assertNotContains($post7->id, $rs2->pluck('id')->toArray());
        $this->assertContains($post8->id, $rs2->pluck('id')->toArray());
        $this->assertNotContains($post9->id, $rs2->pluck('id')->toArray());
    }

    /** @test */
    public function it_can_filter_posts_by_category_id()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Prepare Categories
        $cat1 = $this->createCategoryInPosttype($posttypeBlog);
        $cat2 = $this->createCategoryInPosttype($posttypeBlog);
        $cat3 = $this->createCategoryInPosttype($posttypeBlog);

        // Create Blog Post
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat1->id, $cat2->id]]);
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat2->id, $cat3->id]]);
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat3->id, $cat1->id]]);
        $this->createPostInPosttype($posttypeBlog);
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat3->id]]);
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat2->id]]);
        $this->createPostInPosttype($posttypeBlog, ['categories' => [$cat1->id]]);

        $rs = $this->post->search(['posttype' => 'blog', 'categoryId' => $cat2->id]);

        $this->assertEquals(3, $rs->count());
    }

    /** @test */
    public function it_can_filter_posts_by_tag_slug()
    {
        // Prepare Blog Posttype
        $posttypeBlog = $this->createPosttype(['name' => 'Blog', 'slug' => 'blog']);

        // Create Blog Post
        $post1 = $this->createPostInPosttype($posttypeBlog, ['tags' => ['tag-1', 'tag-2']]);
        $post2 = $this->createPostInPosttype($posttypeBlog, ['tags' => ['tag-3']]);
        $post3 = $this->createPostInPosttype($posttypeBlog, ['tags' => []]);
        $post4 = $this->createPostInPosttype($posttypeBlog, ['tags' => ['tag-3', 'tag-1']]);
        $post5 = $this->createPostInPosttype($posttypeBlog, ['tags' => ['tag-1']]);
        $post6 = $this->createPostInPosttype($posttypeBlog, ['tags' => ['tag-2', 'tag-3']]);

        $rs = $this->post->search(['posttype' => 'blog', 'tags' => 'tag-1']);

        $this->assertEquals(3, $rs->count());
        $this->assertEquals(1, $rs[0]->id);
        $this->assertEquals(4, $rs[1]->id);
        $this->assertEquals(5, $rs[2]->id);
    }
}