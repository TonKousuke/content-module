@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('content::posttypes.title.posttypes') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('content::posttypes.title.posttypes') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.content.posttype.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('content::posttypes.button.create posttype') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="80">ID</th>
                                <th>Name</th>
                                <th width="150">Slug</th>
                                <th width="200" data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($posttypes)): ?>
                            <?php foreach ($posttypes as $posttype): ?>
                            <tr>
                                <td>{{ $posttype->id }}</td>
                                <td>{{ $posttype->name }}</td>
                                <td>{{ $posttype->slug }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.content.posttype.edit', [$posttype->id]) }}" class="btn btn-default btn-flat">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.content.posttype.destroy', [$posttype->id]) }}">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
<?php /*
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
*/ ?>
@stop
@section('shortcuts')
<?php /*
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('content::posttypes.title.create posttype') }}</dd>
    </dl>
*/ ?>
@stop

@push('js-stack')
<?php /*
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.content.posttype.create') ?>" }
                ]
            });
        });
    </script>
*/ ?>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
