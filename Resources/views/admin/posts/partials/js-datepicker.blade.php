@push('js-stack')
<script src="{{ url('/modules/content/vendor/moment/moment.min.js') }}"></script>
<script src="{{ url('/modules/content/vendor/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
$(function(){
    $('.content-datetimepicker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 30,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
    });
    $('.content-datetimepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
    });
    $('.content-datetimepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
@endpush

@push('css-stack')
    <link type="text/css" rel="stylesheet" href="{{ url('/modules/content/vendor/bootstrap-daterangepicker/daterangepicker.css') }}" />
@endpush