<div class="box-body">
    {!! Form::i18nInput('title', 'Title', $errors, $lang, old("$lang.title"), ['autocomplete' => 'off']) !!}

    <div class="form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}">
        <label for="{{ $lang }}[slug]">Slug</label>
        <div class="input-group">
            <div class="input-group-addon">
                {{ url("/content/{$posttype->slug}") }}/{id}_
            </div>
            <input type="text" class="form-control" id="{{ $lang }}[slug]" name="{{ $lang }}[slug]" autocomplete="off" value="{{ old("$lang.slug") }}">
        </div>
        {!! $errors->first("{$lang}.slug", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("{$lang}.short_detail") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[short_detail]", 'Short Detail') !!}
        {!! Form::textarea("{$lang}[short_detail]", old("{$lang}[short_detail]"), ['class' => 'form-control', 'placeholder' => 'Short Detail']) !!}
        {!! $errors->first("{$lang}.short_detail", '<span class="help-block">:message</span>') !!}
    </div>

    {!! Form::i18nTextarea('detail', 'Detail', $errors, $lang, old("$lang.detail"), ['autocomplete' => 'off']) !!}
</div>