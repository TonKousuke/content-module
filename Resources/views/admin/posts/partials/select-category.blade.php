<div id="box-category" class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Categories</h3>
    </div>
    <div class="box-body">
        <?php if (!$categories->isEmpty()) { ?>
            <?php foreach ($categories as $cat) { ?>
                <?php
                    $oldCatInput = old('categories', $selectedCat);
                    $checkedAttr = (in_array($cat->id, $oldCatInput)) ? ' checked="checked"' : '' ;
                ?>
                <div class="checkbox" style="margin-left:{{ $cat->depth*15 }}px;">
                    <label for="cat-{{ $cat->id }}">
                        <input name="categories[]" type="checkbox" value="{{ $cat->id }}" id="cat-{{ $cat->id }}" class="flat-blue" data-parentid="{{ (int) $cat->parent_id }}"<?php echo $checkedAttr ?>> {{ $cat->title }}
                    </label>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

@push('js-stack')
<script>
$(function(){
    $('#box-category input[type="checkbox"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    }).on('ifChecked', function(e){
        var parentId = $(this).data('parentid');
        $('#box-category #cat-' + parentId).iCheck('check');
    }).on('ifUnchecked', function(e){
        var currentId = $(this).val();
        $('#box-category input[data-parentid="' + currentId + '"]').iCheck('uncheck');
    });
});
</script>
@endpush