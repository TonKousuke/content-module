@extends('layouts.master')

@section('content-header')
    <h1>
        Edit a {{ $posttype->name }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.content.post.index', [$posttype->slug]) }}">{{ str_plural($posttype->name) }}</a></li>
        <li class="active">Edit a {{ $posttype->name }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['url' => route('admin.content.post.update', [$posttype->slug, $post->id]), 'method' => 'put', 'id' => 'content-form']) !!}
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('content::admin.posts.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-xs-12 col-md-4">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Post Status</h3>
                </div>
                <div class="box-body">
                    {!! Form:: normalSelect('status', 'Status', $errors, $statusArr, $post) !!}
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Post Schedule</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>Start Date</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input name="start_date" class="form-control pull-right content-datetimepicker" value="{{ old('start_date', $post->start_date) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>End Date</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input name="end_date" class="form-control pull-right content-datetimepicker" value="{{ old('end_date', $post->end_date) }}">
                        </div>
                    </div>
                </div>
            </div>

            @include('content::admin.posts.partials.select-category')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Tags</h3>
                </div>
                <div class="box-body">
                    @tags('Modules\Content\Entities\Post', $post)
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Featured Image</h3>
                </div>
                <div class="box-body">
                    @mediaSingle('thumbnail', $post)
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.content.post.index', [$posttype->slug])}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@include('content::admin.posts.partials.js-auto-slug')
@include('content::admin.posts.partials.js-datepicker')



@section('footer')
<?php /*
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
*/ ?>
@stop
@section('shortcuts')
<?php /*
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
*/ ?>
@stop

@push('js-stack')
<?php /*
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.content.post.index', [$posttype->slug]) ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
*/ ?>
@endpush
