@push('js-stack')
<script>
$(function(){
    $('input[name$="[title]"]').keyup(function(){
        var inputName = $(this).attr('name');
        var regPattern = /^([A-Za-z]+)\[title\]$/g;
        var regExp = new RegExp(regPattern);
        var regExpArr = regExp.exec(inputName);
        var currentLang = regExpArr[1];

        var slugifyTxt = $(this).val().toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-ก-๙]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');
        $('input[name="' + currentLang + '[slug]"]').val( slugifyTxt );
    });
});
</script>
@endpush