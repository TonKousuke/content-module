<?php

$langPostArr = [
    'list resource' => 'List posts',
    'create resource' => 'Create posts',
    'edit resource' => 'Edit posts',
    'destroy resource' => 'Destroy posts',
    'title' => [
        'posts' => 'Post',
        'create post' => 'Create a post',
        'edit post' => 'Edit a post',
    ],
    'button' => [
        'create post' => 'Create a post',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];

$posttypes = \Modules\Content\Entities\Posttype::get();
// $posttypes = app('ContentPosttypesCollection');

foreach ($posttypes as $posttype) {
    $langPostArr["{$posttype->slug} list resource"] = "List {$posttype->name} posts";
    $langPostArr["{$posttype->slug} create resource"] = "Create {$posttype->name} posts";
    $langPostArr["{$posttype->slug} edit resource"] = "Edit {$posttype->name} posts";
    $langPostArr["{$posttype->slug} destroy resource"] = "Destroy {$posttype->name} posts";
}

return $langPostArr;