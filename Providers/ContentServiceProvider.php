<?php

namespace Modules\Content\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Content\Events\Handlers\RegisterContentSidebar;
use Modules\Content\Entities\Post;
use Modules\Tag\Repositories\TagManager;

class ContentServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterContentSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('posts', array_dot(trans('content::posts')));
            $event->load('categories', array_dot(trans('content::categories')));
            $event->load('posttypes', array_dot(trans('content::posttypes')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('content', 'permissions');

        // Register Tag Namespace
        $this->app[TagManager::class]->registerNamespace(new Post());

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        // Reduce Query
        // $posttypes = \Modules\Content\Entities\Posttype::get();
        // $this->app->instance('ContentPosttypesCollection', $posttypes);

        $this->app->bind(
            'Modules\Content\Repositories\PostRepository',
            function () {
                $repository = new \Modules\Content\Repositories\Eloquent\EloquentPostRepository(new \Modules\Content\Entities\Post());
                if (! config('app.cache')) {
                    return $repository;
                }
                return new \Modules\Content\Repositories\Cache\CachePostDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Content\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\Content\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\Content\Entities\Category());
                if (! config('app.cache')) {
                    return $repository;
                }
                return new \Modules\Content\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Content\Repositories\PosttypeRepository',
            function () {
                $repository = new \Modules\Content\Repositories\Eloquent\EloquentPosttypeRepository(new \Modules\Content\Entities\Posttype());
                if (! config('app.cache')) {
                    return $repository;
                }
                return new \Modules\Content\Repositories\Cache\CachePosttypeDecorator($repository);
            }
        );

// add bindings

    }
}
