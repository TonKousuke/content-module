<?php

namespace Modules\Content\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class TagApiTransformer extends Resource
{
    public function toArray($request)
    {
        $tag = $this->resource;

        $responseArr = array_only($tag->toArray(), ['id', 'name', 'slug']);

        return $responseArr;
    }
}
