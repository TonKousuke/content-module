<?php

namespace Modules\Content\Transformers;

use Illuminate\Http\Resources\Json\Resource;
// use Modules\Content\Transformers\CategoryApiTransformer;
use Modules\Content\Transformers\TagApiTransformer;
use Modules\Content\Transformers\CategoryHierarchyApiTransformer;

class PostApiTransformer extends Resource
{
    public function toArray($request)
    {
        $post = $this->resource;
        $post->setAppends(['permalink']);
        $post->load(['categories', 'tags']);

        $responseArr = array_except($post->toArray(), ['posttype_id', 'translations', 'categories', 'tags']);

        // Categories
        // $responseArr['categories'] = CategoryApiTransformer::collection($post->categories);
        $categoriesHierarchyTransformed = CategoryHierarchyApiTransformer::collection($post->categories->toHierarchy());
        $responseArr['categories'] = array_values($categoriesHierarchyTransformed->collection->toArray());

        // Tags
        $responseArr['tags'] = TagApiTransformer::collection($post->tags);

        // Get Thumbnail Image (All Size)
        $thumbnail = $post->filesByZone('thumbnail')->first();
        if (!empty($thumbnail)) {
            $responseArr['thumbnail'] = [
                'original'  => asset($thumbnail->getOriginal('path')),
                'square'    => \Imagy::getThumbnail($thumbnail->path, 'squareThumb'),
                'rectangle' => \Imagy::getThumbnail($thumbnail->path, 'rectangleThumb'), 
            ];
        } else {
            $responseArr['thumbnail'] = [
                'original'  => '',
                'square'    => '',
                'rectangle' => '', 
            ];
        }
        

        return $responseArr;
    }
}
