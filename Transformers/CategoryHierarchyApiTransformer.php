<?php

namespace Modules\Content\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CategoryHierarchyApiTransformer extends Resource
{
    public function toArray($request)
    {
        $category = $this->resource;
        $category->setAppends(['permalink']);
        $responseArr = array_only($category->toArray(), ['id', 'parent_id', 'title', 'slug', 'detail', 'children', 'permalink']);

        $responseArr['children'] = CategoryHierarchyApiTransformer::collection($category->children);

        return $responseArr;
    }
}
